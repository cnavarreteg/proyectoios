//
//  BagPackTableViewController.swift
//  MovieBox
//
//  Created by Cristobal Navarrete on 30/7/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import UIKit

class BagPackTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
let itemManager = ItemManager()
    
    @IBOutlet weak var itemsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        itemManager.updateArrays()
        itemsTableView.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return itemManager.toDoItems.count
        }
        return itemManager.doneItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! ItemTableViewCell
        
        if indexPath.section == 0 {
            cell.titleLabel.text = itemManager.toDoItems[indexPath.row].title
            cell.locationLabel.text = itemManager.toDoItems[indexPath.row].overview
            
        } else {
            cell.titleLabel.text = itemManager.doneItems[indexPath.row].title
            cell.locationLabel.text = ""
        }
        
        return cell
    }
    
   /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            performSegue(withIdentifier: "toItemInfoSegue", sender: self)
        }
    }*/
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "To Do" : "Done"
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return indexPath.section == 0 ? "Check" : "UnCheck"
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            itemManager.checkItem(index: indexPath.row)
        } else {
            itemManager.unCheckItem(index: indexPath.row)
        }
        itemManager.updateArrays()
        itemsTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toItemtableViewCellSegue" {
            //let destination = segue.destination as! ItemTableViewCell
            //destination.itemManager = itemManager
         }
        if segue.identifier == "toItemInfoSegue" {
            let destination = segue.destination as! ItemInfoViewController
            let selectedRow = itemsTableView.indexPathsForSelectedRows![0]
            destination.itemInfo = (itemManager, selectedRow.row)
        }
    }
    
    
}
