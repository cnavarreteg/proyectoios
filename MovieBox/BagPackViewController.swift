//
//  BagPackViewController.swift
//  MovieBox
//
//  Created by Cristobal Navarrete on 30/7/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import UIKit

class BagPackViewController: UIViewController {
  var itemManager = ItemManager()
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var txtComentario: UITextField!

    @IBOutlet weak var txtCalificar: UITextField!
    var overview1 = ""
    var title1 = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = title1
        overviewLabel.text = overview1
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        let itemTitle = titleLabel.text ?? ""
        let itemOverview = overviewLabel.text ?? ""
        let itemDescription = txtComentario.text ?? ""
        let itemCalificar = txtCalificar.text ?? ""
        
        if itemTitle == "" {
            showAlert(title: "Error", message: "Title is required")
        }
        
        //    itemManager?.toDoItems += [item]
        
        itemManager.addItem(
            title: itemTitle,
            overview: itemOverview,
            calificar: itemCalificar,
            itemDescription: itemDescription)
        
        navigationController?.popViewController(animated: true)
        print(itemTitle)
        print(itemOverview)
    }
    
    func showAlert(title:String, message:String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toTable" {
            //let destination = segue.destination as! BagPackTabeViewController
            
            //let selectedRow = movieTableView.indexPathsForSelectedRows![0]
            //destination.itemInfoMovie = (movieManager, selectedRow.row)
        }
    }
    
    }
    


