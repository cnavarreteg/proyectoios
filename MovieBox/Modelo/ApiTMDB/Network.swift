//
//  Network.swift
//  MovieBox
//
//  Created by Ronny Cabrera on 24/7/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class Network {
    
    func getALLMovie(of name: String, completion:@escaping ([MovieInfo])->()) {
        var movieArray:[MovieInfo] = []
        
      
            Alamofire.request("https://api.themoviedb.org/3/search/movie?api_key=17124aceb9febb58c209a044aff54c9b&query=\(name))").responseJSON {
                response in
                
                guard let data = response.data else {
                    print("error")
                    return
                }
                
                guard let movie = try? JSONDecoder().decode(Movie.self, from: data)else {
                    print("error decoding Movie")
                    return
                }
                
                for movies in movie.results {
                    let movieAux = MovieInfo(title: movies.title, overview: movies.overview, poster_path: movies.poster_path)
                    movieArray.append(movieAux)
                }
                completion(movieArray)
            }
    }
    
    func getMovieImage(image:String, completion:@escaping (UIImage)->()){
        
        let url = "https://image.tmdb.org/t/p/w342/\(image)"
        print(url)
        Alamofire.request(url).responseImage { response in debugPrint(response)
            
            if let image = response.result.value {
                completion(image)
            }
        }
    }
}
