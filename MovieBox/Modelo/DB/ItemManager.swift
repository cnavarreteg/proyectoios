//
//  ItemManager.swift
//  ToDo
//
//  Created by Sebastian Guerrero F on 5/9/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation
import RealmSwift

class ItemManager {
  
  var toDoItems:[Item] = []
  var doneItems:[Item] = []
  var realm:Realm
  
  init() {
    realm = try! Realm()
    print(realm.configuration.fileURL)
  }
  
  func checkItem(index:Int){
   
    let item = toDoItems[index]
    
    try! realm.write {
      item.done = true
    }
  }
  
  func unCheckItem(index:Int){

    let item = doneItems[index]
    
    try! realm.write {
      item.done = false
    }
    
  }
  
    func addItem(title:String, overview:String, calificar:String, itemDescription:String?) {
    let item = Item()
    item.id = "\(UUID())"
    item.title = title
    item.overview = overview
    item.itemDescription = itemDescription
    item.calificar = calificar
    try! realm.write {
      realm.add(item)
    }
    

  }
  
  func updateArrays() {
    toDoItems = Array(realm.objects(Item.self).filter("done = false"))
    doneItems = Array(realm.objects(Item.self).filter("done = true"))
  }
}


























