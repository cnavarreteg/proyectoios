//
//  InformationViewController.swift
//  MovieBox
//
//  Created by Ronny Cabrera on 7/27/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import UIKit

class InformationViewController: UIViewController {

    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    var itemInfoMovie:(itemManagerMovie: MovieManager,index: Int)?
    //var title1 : String!
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = itemInfoMovie?.itemManagerMovie.itemsMovie[(itemInfoMovie?.index)!].title
        
        overviewLabel.text = itemInfoMovie?.itemManagerMovie.itemsMovie[(itemInfoMovie?.index)!].overview
        //title1 = titleLabel.text!
        let url = itemInfoMovie?.itemManagerMovie.itemsMovie[(itemInfoMovie?.index)!].poster_path
        
        let bm = Network()
        bm.getMovieImage(image: url!) {
            (pkImage) in
            self.movieImageView.image = pkImage
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toBagPackSegue" {
            let destination = segue.destination as! BagPackViewController
            destination.title1 =  titleLabel.text!
            destination.overview1 = overviewLabel.text!
            //let selectedRow = movieTableView.indexPathsForSelectedRows![0]
            //destination.itemInfoMovie = (movieManager, selectedRow.row)
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
