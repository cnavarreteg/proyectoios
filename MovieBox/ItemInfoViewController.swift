//
//  ItemInfoViewController.swift
//  ToDo
//
//  Created by Cristobal Navarrete F on 5/16/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import UIKit

class ItemInfoViewController: UIViewController {
   


    
    @IBOutlet weak var calificarLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var overviewLabel: UILabel!
    var itemInfo:(itemManager: ItemManager, index: Int)?
    @IBOutlet weak var descriptionLabel: UILabel!
    
  override func viewDidLoad() {
    super.viewDidLoad()
    titleLabel.text =
      itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].title
    overviewLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].overview
    descriptionLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].itemDescription
    calificarLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].calificar
  }
  
  @IBAction func checkButtonPressed(_ sender: Any) {
   // itemInfo?.itemManager.checkItem(index: (itemInfo?.index)!)
    //navigationController?.popViewController(animated: true)
  }
  
  
}









