//
//  ItemTableViewCell.swift
//  ToDo
//
//  Created by Cristobal Navarrete F on 5/22/18.
//  Copyright © 2018 Cristobal Navarrete . All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
  
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
}
